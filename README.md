# Mentoria React

### Instruções

- Criar um repositorio no seu Github, pode ser publico, subir todas as features em formato de PR's
- Usar React / SCSS ou Styled Components para a construção da aplicação/components.
- Inicialmente para o controle de estado global utilizar as API's do proprio react e em uma proxima fase refatoraremos para usar o Redux Thunk.
- Seguir o seguinte design [usar esse link](https://xd.adobe.com/spec/f6e71782-ebba-4573-6f7a-005a1a6d391f-80d6/)

### Features propostas

- Consulte a API disponibilizada para buscar as informações.
- Crie uma tela para exibir a lista de restaurantes:
- O usuário deve ser capaz de buscar por estabelecimento.
- Indique se cada um deles está aberto ou fechado sem ser necessário recarregar ou reabrir a página.
- Para cada restaurante, deve ser exibido os horários de funcionamento, as promoções ativas no momento e o cardápio.
- O restaurante deve atualizar o status de aberto/fechado, de acordo com o horário de funcionamento, sem ser necessário recarregar ou reabrir a página.
- Crie uma tela para exibir os produto do cardápio de cada um dos restaurantes:
- O usuário deve ser capaz de buscar os produtos.
- Para os produtos com promoção ativa, deve ser exibido o valor original e o valor promocional.
- As promoções ativas e o valor promocional devem ser atualizados na interface, de acordo com o horário, sem a necessidade de recarregar ou reabrir a página.

Formato de horários
- É necessário tratar os campos que indicam horários de funcionamento.
- Os campos from e to possuem o formato HH:mm.
- Caso o campo to possua um horário anterior ao valor de from, deve-se considerar que o horário se estende até o horário contido em to do próximo dia. Por exemplo, se from for 19:00 e to for 02:00, o horário a ser considerado é das 19h do dia atual até às 02h do dia seguinte.
- O campo days guarda os dias da semana em que o horário é válido. Sendo Domingo o valor 1 e Sábado o valor 7. Os horários possuem intervalo mínimo de 15 minutos.

### API para consumo

https://challange.goomer.com.br os endpoints você encontra aqui.

**Examplos de consulta na API:**

- Busca de restaurantes - http://challange.goomer.com.br/restaurants
```javascript
// GET: http://challange.goomer.com.br/restaurants

// Formato de Resposta:
[
  {
    "id": Number,
    "name": String,
    "address": String,
    "image": String,
    "hours:[
      {
        "from": String,
        "to": String,
        "days": [Number]
      }
    ]
  }
]
```

- Busca de cardápio de um restaurante - http://challange.goomer.com.br/restaurants/{id}/menu
```javascript
// GET: http://challange.goomer.com.br/restaurants/{id}/menu

// Formato de resposta:
[
  {
    "restaurantId": Number,
    "name": String,
    "image": String,
    "price": Number,
    "group": String,
    "sales": [
      {
        "description": String,
        "price": Number,
        "hours": [
          {
            "from": String,
            "to": String,
            "days": [Number]
          }
        ]
      }
    ]
  }
]
```
